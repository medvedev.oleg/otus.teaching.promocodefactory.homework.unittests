﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace StateMachine.Compose
{
    public static class Configuration
    {
        public static IServiceCollection GetServiceCollection(IConfigurationRoot configuration, string serviceName, IServiceCollection serviceCollection = null)
        {
            if (serviceCollection == null)
            {
                serviceCollection = new ServiceCollection();
            }
            serviceCollection
                .AddSingleton(configuration)
                .AddSingleton((IConfiguration)configuration)
               // .ConfigureAutomapper()
                .ConfigureAllRepositories()
                //.ConfigureAllBusinessServices()
                .AddLogging(builder =>
                {
                    builder.ClearProviders();
                   // builder.AddConfiguration(configuration.GetSection("Logging"));
                    builder
                        .AddFilter("Microsoft", LogLevel.Warning)
                        .AddFilter("System", LogLevel.Warning);
                })
                .AddMemoryCache();
            return serviceCollection;
        }

        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            services.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDb", builder => { });
                options.UseInternalServiceProvider(serviceProvider);
            });
            services.AddTransient<DbContext, DataContext>();
            return services;
        }


        //private static IServiceCollection ConfigureAutomapper(this IServiceCollection services) => services
        //    .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));

        private static IServiceCollection ConfigureAllRepositories(this IServiceCollection services) => services
            .AddTransient(typeof(IRepository<>), typeof(EfRepository<>));
 
        //private static IServiceCollection ConfigureAllBusinessServices(this IServiceCollection services) => services
        //    .AddTransient<ILotService, LotService>()
        //    .AddTransient<ILotManager, LotManager>()
        //    .AddTransient<ILotDocumentService, LotDocumentService>();

        //private static MapperConfiguration GetMapperConfiguration()
        //{
        //    var configuration = new MapperConfiguration(cfg =>
        //    {
        //        cfg.AddProfile<LotManagerMapProfiles>();
        //    });
        //    configuration.AssertConfigurationIsValid();
        //    return configuration;
        //}
    }
}
