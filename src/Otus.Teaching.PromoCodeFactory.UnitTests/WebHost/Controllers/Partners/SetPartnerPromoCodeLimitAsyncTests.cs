﻿using System;
using Xunit;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using AutoFixture.AutoMoq;
using AutoFixture;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System.Collections.Generic;
using StateMachine.BusinessLogic.Managers.UnitTests_Demo;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.UnitTests.Exceptions;
using System.Linq;
using MassTransit.Internals.GraphValidation;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    /// <summary>
    ///Нужно протестировать следующие Test Cases для установки партнеру (класс Partner)
    ///лимита (класс PartnerLimit) метод SetPartnerPromoCodeLimitAsync в PartnersController):
    /// </summary>
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixture_InMemory>
    {
        private Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private IRepository<Partner> _partnersRepository;

        public SetPartnerPromoCodeLimitAsyncTests(TestFixture_InMemory testFixture)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

            var serviceProvider = testFixture.ServiceProvider;
            _partnersRepository = serviceProvider.GetService<IRepository<Partner>>();
        }

        public Partner CreateBasePartner(Guid? Id)
        {
            var partner = new Partner()
            {
                Id = Id ?? Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Реплика Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.NewGuid(),
//                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public SetPartnerPromoCodeLimitRequest CreateBaseRequest()
        {
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 5
            };

            return request;
        }

        /// <summary>
        /// Общая логика для работы с лимитом и промокодами ...
        /// 3.Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        /// 4.При установке лимита нужно отключить предыдущий лимит;
        /// 5.Лимит должен быть больше 0;    
        /// </summary>
        public async Task BaseLogicSetPartnerPromoCodeLimitAsync(Partner partner, SetPartnerPromoCodeLimitRequest request)
        {
            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue, null);

            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }
 
            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = request.EndDate
            };

            partner.PartnerLimits.Add(newLimit);
            await _partnersRepository.UpdateAsync(partner);

        }

        /// <summary>
        /// 1.Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerIsNotFound_True()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = null;

            var request = CreateBaseRequest();

            _partnersRepositoryMock.Setup(repository => repository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }


        /// <summary>
        /// 2.Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerIsNotActive_True()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            //Успешный
            var partner = CreateBasePartner(partnerId);
            partner.IsActive = false;

            var request = CreateBaseRequest();

            _partnersRepositoryMock.Setup(repository => repository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert      
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 3.Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;  
        /// > Проверяю сброс NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerSetLimitWeResetNumberPromoCodes_True()
        {
            //Arrange 
            int NUMBER_ISSUED_PROMO_CODES = 3;
            var partnerId = Guid.NewGuid();
            var request = CreateBaseRequest();

            //Добавляем партнера в имимтацию БД
            var basePartner = CreateBasePartner(partnerId);

            basePartner.NumberIssuedPromoCodes = NUMBER_ISSUED_PROMO_CODES;
            await _partnersRepository.AddAsync(basePartner);

            //Act                        
            var testingPartner = await _partnersRepository.GetByIdAsync(basePartner.Id);

            await BaseLogicSetPartnerPromoCodeLimitAsync(testingPartner, request);

            var result = await _partnersRepository.GetByIdAsync(basePartner.Id);
            ////

            ////Assert
            Assert.Equal(0, result.NumberIssuedPromoCodes);
        }

        /// <summary>
        /// 3.Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;  
        /// > Проверяю что значение NumberIssuedPromoCodes не сбросилось
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerSetLimitAndLimitEndedWeNotСhangeNumberPromoCodes_True()
        {
            //Arrange 
            int NUMBER_ISSUED_PROMO_CODES = 3;
            var partnerId = Guid.NewGuid();
            var request = CreateBaseRequest();

            //Добавляем партнера в имимтацию БД
            var basePartner = CreateBasePartner(partnerId);
            basePartner.NumberIssuedPromoCodes = NUMBER_ISSUED_PROMO_CODES; 
            basePartner.PartnerLimits.FirstOrDefault().CancelDate = DateTime.Now; 

            await _partnersRepository.AddAsync(basePartner);

            //Act                        
            var testingPartner = await _partnersRepository.GetByIdAsync(basePartner.Id);
            await BaseLogicSetPartnerPromoCodeLimitAsync(testingPartner, request);

            var result = await _partnersRepository.GetByIdAsync(basePartner.Id);
            ////

            ////Assert
            Assert.Equal(NUMBER_ISSUED_PROMO_CODES, result.NumberIssuedPromoCodes);
        }


        /// <summary> 
        /// 4.При установке лимита нужно отключить предыдущий лимит;     
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfSetLimitUpdatePreviousLimit_True()
        {
            //Arrange 
            var partnerId = Guid.NewGuid();
            var request = CreateBaseRequest();

            //Добавляем партнера в имимтацию БД
            var basePartner = CreateBasePartner(partnerId);
            var testingPartnerLimit = basePartner.PartnerLimits.FirstOrDefault();
            await _partnersRepository.AddAsync(basePartner);

            //Act                        
            var testingPartner = await _partnersRepository.GetByIdAsync(basePartner.Id);
            await BaseLogicSetPartnerPromoCodeLimitAsync(testingPartner, request);
            var result = await _partnersRepository.GetByIdAsync(basePartner.Id);
            ////

            ////Assert            
            Assert.NotNull(result.PartnerLimits.FirstOrDefault(l => l.Id == testingPartnerLimit.Id).CancelDate);
        }

        /// <summary>      
        /// 5.Лимит должен быть больше 0;        
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfLimitEqualZero_True()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = CreateBasePartner(partnerId);

            var request = CreateBaseRequest();
            request.Limit = 0;

            _partnersRepositoryMock.Setup(repository => repository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 6.Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);        
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerLimitsAdded_True()
        {
            //Arrange 
            var partnerId = Guid.NewGuid();
            var request = CreateBaseRequest();

            //Добавляем партнера в имимтацию БД
            var basePartner = CreateBasePartner(partnerId);
            await _partnersRepository.AddAsync(basePartner);
            ////


            //Act 
            var testingPartner = await _partnersRepository.GetByIdAsync(basePartner.Id);
            await BaseLogicSetPartnerPromoCodeLimitAsync(testingPartner, request);
             
            var result = await _partnersRepository.GetByIdAsync(basePartner.Id);
            ////

            ////Assert
            Assert.Equal(2, result.PartnerLimits.Count);
        }

    }
}