﻿
using System;
using System.Runtime.Serialization;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Exceptions
{

    [Serializable]
    public class PartnerNotFoundException : Exception
    {
        public PartnerNotFoundException(Guid id) : base($"Ошибка 404. Партнер {id} не найден")
        {
        }

        public PartnerNotFoundException(SerializationInfo serializationInfo, StreamingContext streamingContext)
       : base(serializationInfo, streamingContext)
        {
        }
    }
}
