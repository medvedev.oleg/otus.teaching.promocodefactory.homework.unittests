﻿
using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Exceptions
{

    [Serializable]
    public class PartnerNotActiveException : Exception
    {
        public PartnerNotActiveException(Guid id) : base($"Ошибка 404. Партнер {id} заблокирован")
        {
             
        }

        public PartnerNotActiveException(SerializationInfo serializationInfo, StreamingContext streamingContext)
       : base(serializationInfo, streamingContext)
        {
        }
    }
}
